#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(dead_code)]

#[macro_use]
extern crate bitflags;
extern crate libc;
extern crate shared_library;
#[macro_use]
extern crate dvk;

use self::libc::{c_void, c_char, uint32_t, size_t, uint64_t, c_float, int32_t, uint8_t};
use self::shared_library::dynamic_library::DynamicLibrary;
use std::path::{Path};
use std::ffi::CString;
use dvk::*;

VK_DEFINE_NON_DISPATCHABLE_HANDLE!(VkSurfaceKHR);

pub const VK_KHR_SURFACE_SPEC_VERSION: uint32_t = 25;
pub const VK_KHR_SURFACE_EXTENSION_NAME: *const c_char = b"VK_KHR_surface\0" as *const u8 as *const c_char;
pub const VK_COLORSPACE_SRGB_NONLINEAR_KHR: VkColorSpaceKHR = VkColorSpaceKHR::VK_COLOR_SPACE_SRGB_NONLINEAR_KHR;

#[repr(i32)]
#[derive(Eq)]
#[derive(PartialEq)]
#[derive(Debug)]
pub enum VkKhrSurfaceResult {
    VK_SUCCESS = 0,
    //VK_NOT_READY = 1,
    //VK_TIMEOUT = 2,
    //VK_EVENT_SET = 3,
    //VK_EVENT_RESET = 4,
    VK_INCOMPLETE = 5,
    VK_ERROR_OUT_OF_HOST_MEMORY = -1,
    VK_ERROR_OUT_OF_DEVICE_MEMORY = -2,
    //VK_ERROR_INITIALIZATION_FAILED = -3,
    //VK_ERROR_DEVICE_LOST = -4,
    //VK_ERROR_MEMORY_MAP_FAILED = -5,
    //VK_ERROR_LAYER_NOT_PRESENT = -6,
    //VK_ERROR_EXTENSION_NOT_PRESENT = -7,
    //VK_ERROR_FEATURE_NOT_PRESENT = -8,
    //VK_ERROR_INCOMPATIBLE_DRIVER = -9,
    //VK_ERROR_TOO_MANY_OBJECTS = -10,
    //VK_ERROR_FORMAT_NOT_SUPPORTED = -11,
    VK_ERROR_SURFACE_LOST_KHR = -1000000000,
    VK_ERROR_NATIVE_WINDOW_IN_USE_KHR = -1000000001
}

#[repr(i32)]
#[derive(Eq)]
#[derive(PartialEq)]
#[derive(Debug)]
pub enum VkColorSpaceKHR {
    VK_COLOR_SPACE_SRGB_NONLINEAR_KHR = 0
}

#[repr(i32)]
#[derive(Eq)]
#[derive(PartialEq)]
#[derive(Debug)]
pub enum VkPresentModeKHR {
    VK_PRESENT_MODE_IMMEDIATE_KHR = 0,
    VK_PRESENT_MODE_MAILBOX_KHR = 1,
    VK_PRESENT_MODE_FIFO_KHR = 2,
    VK_PRESENT_MODE_FIFO_RELAXED_KHR = 3
}

bitflags! {
    pub flags VkSurfaceTransformFlagBitsKHR: VkFlags {
        const VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR = 0x00000001,
        const VK_SURFACE_TRANSFORM_ROTATE_90_BIT_KHR = 0x00000002,
        const VK_SURFACE_TRANSFORM_ROTATE_180_BIT_KHR = 0x00000004,
        const VK_SURFACE_TRANSFORM_ROTATE_270_BIT_KHR = 0x00000008,
        const VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_BIT_KHR = 0x00000010,
        const VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_ROTATE_90_BIT_KHR = 0x00000020,
        const VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_ROTATE_180_BIT_KHR = 0x00000040,
        const VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_ROTATE_270_BIT_KHR = 0x00000080,
        const VK_SURFACE_TRANSFORM_INHERIT_BIT_KHR = 0x00000100
    }
}

pub type VkSurfaceTransformFlagsKHR = VkFlags;

bitflags! { 
    pub flags VkCompositeAlphaFlagBitsKHR: VkFlags {
        const VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR = 0x00000001,
        const VK_COMPOSITE_ALPHA_PRE_MULTIPLIED_BIT_KHR = 0x00000002,
        const VK_COMPOSITE_ALPHA_POST_MULTIPLIED_BIT_KHR = 0x00000004,
        const VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR = 0x00000008
    }
}

pub type VkCompositeAlphaFlagsKHR = VkFlags;

#[repr(C)]
pub struct VkSurfaceCapabilitiesKHR {
    pub minImageCount: uint32_t,
    pub maxImageCount: uint32_t,
    pub currentExtent: VkExtent2D,
    pub minImageExtent: VkExtent2D,
    pub maxImageExtent: VkExtent2D,
    pub maxImageArrayLayers: uint32_t,
    pub supportedTransforms: VkSurfaceTransformFlagsKHR,
    pub currentTransform: VkSurfaceTransformFlagBitsKHR,
    pub supportedCompositeAlpha: VkCompositeAlphaFlagsKHR,
    pub supportedUsageFlags: VkImageUsageFlags
}

#[repr(C)]
pub struct VkSurfaceFormatKHR {
    pub format: VkFormat,
    pub colorSpace: VkColorSpaceKHR
}

pub type vkDestroySurfaceKHRFn = unsafe extern "stdcall" fn(instance: VkInstance,
                                                            surface: VkSurfaceKHR,
                                                            pAllocator: *const VkAllocationCallbacks);

pub type vkGetPhysicalDeviceSurfaceSupportKHRFn = unsafe extern "stdcall" fn(physicalDevice: VkPhysicalDevice,
                                                                             queueFamilyIndex: uint32_t,
                                                                             surface: VkSurfaceKHR,
                                                                             pSupported: *mut VkBool32) -> VkKhrSurfaceResult;

pub type vkGetPhysicalDeviceSurfaceCapabilitiesKHRFn = unsafe extern "stdcall" fn(physicalDevice: VkPhysicalDevice,
                                                                                  surface: VkSurfaceKHR,
                                                                                  pSurfaceCapabilities: *mut VkSurfaceCapabilitiesKHR) -> VkKhrSurfaceResult;

pub type vkGetPhysicalDeviceSurfaceFormatsKHRFn = unsafe extern "stdcall" fn(physicalDevice: VkPhysicalDevice,
                                                                             surface: VkSurfaceKHR,
                                                                             pSurfaceFormatCount: *mut uint32_t,
                                                                             pSurfaceFormats: *mut VkSurfaceFormatKHR) -> VkKhrSurfaceResult;

pub type vkGetPhysicalDeviceSurfacePresentModesKHRFn = unsafe extern "stdcall" fn(physicalDevice: VkPhysicalDevice,
                                                                                  surface: VkSurfaceKHR,
                                                                                  pPresentModeCount: *mut uint32_t,
                                                                                  pPresentModes: *mut VkPresentModeKHR) -> VkKhrSurfaceResult;

#[cfg(windows)]
static VULKAN_LIBRARY: &'static str = "vulkan-1.dll";

#[cfg(unix)]
static VULKAN_LIBRARY: &'static str = "libvulkan-1.so";

#[derive(Default)]
pub struct VulkanKhrSurface {
   library: Option<DynamicLibrary>,
   vkGetInstanceProcAddr: Option<vkGetInstanceProcAddrFn>,
   vkDestroySurfaceKHR: Option<vkDestroySurfaceKHRFn>,
   vkGetPhysicalDeviceSurfaceSupportKHR: Option<vkGetPhysicalDeviceSurfaceSupportKHRFn>,
   vkGetPhysicalDeviceSurfaceCapabilitiesKHR: Option<vkGetPhysicalDeviceSurfaceCapabilitiesKHRFn>,
   vkGetPhysicalDeviceSurfaceFormatsKHR: Option<vkGetPhysicalDeviceSurfaceFormatsKHRFn>,
   vkGetPhysicalDeviceSurfacePresentModesKHR: Option<vkGetPhysicalDeviceSurfacePresentModesKHRFn>
}

impl VulkanKhrSurface {
    pub fn new() -> Result<VulkanKhrSurface, String> {
        let mut vulkan_khr_surface: VulkanKhrSurface = Default::default();
        let library_path = Path::new(VULKAN_LIBRARY);
        vulkan_khr_surface.library = match DynamicLibrary::open(Some(library_path)) {
            Err(error) => return Err(format!("Failed to load {}: {}",VULKAN_LIBRARY,error)),
            Ok(library) => Some(library),
        };
        unsafe {
            vulkan_khr_surface.vkGetInstanceProcAddr = Some(std::mem::transmute(try!(vulkan_khr_surface.library.as_ref().unwrap().symbol::<u8>("vkGetInstanceProcAddr"))));
        }
        Ok(vulkan_khr_surface)
    }

    unsafe fn load_command(&self, instance: VkInstance, name: &str) -> Result<vkVoidFunctionFn, String> {
        let fn_ptr = (self.vkGetInstanceProcAddr.as_ref().unwrap())(instance, CString::new(name).unwrap().as_ptr());
        if fn_ptr != std::ptr::null() {
            Ok(fn_ptr)
        } else {
            Err(format!("Failed to load {}",name))
        }
    }

    pub fn load(&mut self, instance: VkInstance) -> Result<(), String> {
        unsafe {
            self.vkDestroySurfaceKHR = Some(std::mem::transmute(try!(self.load_command(instance, "vkDestroySurfaceKHR"))));
            self.vkGetPhysicalDeviceSurfaceSupportKHR = Some(std::mem::transmute(try!(self.load_command(instance, "vkGetPhysicalDeviceSurfaceSupportKHR"))));
            self.vkGetPhysicalDeviceSurfaceCapabilitiesKHR = Some(std::mem::transmute(try!(self.load_command(instance, "vkGetPhysicalDeviceSurfaceCapabilitiesKHR"))));
            self.vkGetPhysicalDeviceSurfaceFormatsKHR = Some(std::mem::transmute(try!(self.load_command(instance, "vkGetPhysicalDeviceSurfaceFormatsKHR"))));
            self.vkGetPhysicalDeviceSurfacePresentModesKHR = Some(std::mem::transmute(try!(self.load_command(instance, "vkGetPhysicalDeviceSurfacePresentModesKHR"))));
        }
        Ok(())
    }

    pub unsafe fn vkDestroySurfaceKHR(&self, 
                                      instance: VkInstance,
                                      surface: VkSurfaceKHR,
                                      pAllocator: *const VkAllocationCallbacks) {
        (self.vkDestroySurfaceKHR.as_ref().unwrap())(instance, surface, pAllocator)
    }

    pub unsafe fn vkGetPhysicalDeviceSurfaceSupportKHR(&self, 
                                                       physicalDevice: VkPhysicalDevice,
                                                       queueFamilyIndex: uint32_t,
                                                       surface: VkSurfaceKHR,
                                                       pSupported: *mut VkBool32) -> VkKhrSurfaceResult {
        (self.vkGetPhysicalDeviceSurfaceSupportKHR.as_ref().unwrap())(physicalDevice, queueFamilyIndex, surface, pSupported)
    }
    
    pub unsafe fn vkGetPhysicalDeviceSurfaceCapabilitiesKHR(&self, 
                                                            physicalDevice: VkPhysicalDevice,
                                                            surface: VkSurfaceKHR,
                                                            pSurfaceCapabilities: *mut VkSurfaceCapabilitiesKHR) -> VkKhrSurfaceResult {
        (self.vkGetPhysicalDeviceSurfaceCapabilitiesKHR.as_ref().unwrap())(physicalDevice, surface, pSurfaceCapabilities)
    }
    
    pub unsafe fn vkGetPhysicalDeviceSurfaceFormatsKHR(&self, 
                                                       physicalDevice: VkPhysicalDevice,
                                                       surface: VkSurfaceKHR,
                                                       pSurfaceFormatCount: *mut uint32_t,
                                                       pSurfaceFormats: *mut VkSurfaceFormatKHR) -> VkKhrSurfaceResult {
        (self.vkGetPhysicalDeviceSurfaceFormatsKHR.as_ref().unwrap())(physicalDevice, surface, pSurfaceFormatCount, pSurfaceFormats)
    }
    
    pub unsafe fn vkGetPhysicalDeviceSurfacePresentModesKHR(&self, 
                                                            physicalDevice: VkPhysicalDevice,
                                                            surface: VkSurfaceKHR,
                                                            pPresentModeCount: *mut uint32_t,
                                                            pPresentModes: *mut VkPresentModeKHR) -> VkKhrSurfaceResult {
        (self.vkGetPhysicalDeviceSurfacePresentModesKHR.as_ref().unwrap())(physicalDevice, surface, pPresentModeCount, pPresentModes)
    }

}

